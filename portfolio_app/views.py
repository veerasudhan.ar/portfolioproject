from django.http.response import HttpResponseRedirect, HttpResponse
from django.urls.base import reverse
from portfolio_app.models import ProjectInfo
from django.shortcuts import render
from portfolio_app.forms import ProjectInfoForm,UserForm
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
# Create your views here.

def index(request):
    return render(request, 'portfolio_app/index.html')


def project_detail(request,i):
    project = ProjectInfo.objects.get(id=i)
    context = {'project_detail': project}
    return render(request, 'portfolio_app/project_detail.html', context)

def project_data(request):
    
    if request.method == 'POST':
        addproject =ProjectInfoForm(data=request.POST,files=request.FILES)
        if addproject.is_valid():
            addproject.save()
            return HttpResponseRedirect(reverse('projectPage'))

        else:
            print(addproject.errors)
     
    else:
        addproject =ProjectInfoForm()
    return render(request,'portfolio_app/project_data.html',{'addproject': addproject})


def deleteView(request,i):
    y = ProjectInfo.objects.get(id= i)
    y.delete()
    return HttpResponseRedirect(reverse('projectPage'))



@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))



def register(request):
     registered = False
     
     if request.method == "POST":
         user_form = UserForm(data=request.POST)
         

         if user_form.is_valid():
             user =user_form.save()
             user.set_password(user.password)
             user.save()

             registered = True
         else:
            print(user_form.errors)
     else:
         user_form = UserForm()
         
    
     return render(request,'portfolio_app/signup.html',{'user_form': user_form,'registered':registered})


def user_login(request):
    if request.method == 'POST':
        username =request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username,password=password)

        if user:
            if user.is_active:
                login(request,user)
                return HttpResponseRedirect(reverse('projectPage'))

            else:
                return HttpResponse("Account Not Active")
        else:
            print("Username: {} and password {}".format(username,password))
            return HttpResponse("invalid login details supplied!")

    else:
        return render(request,'portfolio_app/login.html',{})


def projectPage(request):
    projects = ProjectInfo.objects.all()
    context = {'projects': projects}
    return render(request, 'portfolio_app/projectPage.html', context)

