from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class ProjectInfo(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    technology = models.CharField(max_length=20)
    image = models.ImageField(upload_to='img')

class portfolioList(models.Model):
     user = models.OneToOneField(User,on_delete=models.CASCADE,)   