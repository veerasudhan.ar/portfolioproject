from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'portfolio_app'
urlpatterns = [
     url(r'^user_login/$',views.user_login,name='user_login'),
    url(r'^project_data/$',views.project_data,name ="project_data"),
    url(r'^signup/$',views.register,name ='signup'),
]