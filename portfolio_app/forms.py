from django import forms
from django.contrib.auth.models import User
from portfolio_app.models import ProjectInfo,portfolioList


class ProjectInfoForm(forms.ModelForm):
    class Meta():
        model = ProjectInfo
        fields = ('title','description','technology','image')

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta():
        model = User
        fields = ('username','email','password')